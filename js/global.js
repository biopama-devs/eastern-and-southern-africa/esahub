/**
 * @file
 * Global utilities.
 *
 */

(function ($, Drupal) {

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			document.getElementById("scrollTop").style.display = "block";
		} else {
			document.getElementById("scrollTop").style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}

	window.onscroll = function () { scrollFunction() };

	var scrollTop = '<div id="scrollTop">' +
		'<button class="btn btn-success rounded-circle" title="Go to top"><i class="fas fa-arrow-up fa-2x"></i></button>' +
		'</div>';
	$("body").append(scrollTop);

	$('#scrollTop').on('click', function () { topFunction(); });


	Drupal.behaviors.closeAlerts = {
		attach: function (context, settings) {
			$("button.close").click(function (event) {
				$(this).closest('div.alert').remove();
			});
			var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]:not(.tooltipified)'))
			var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
				$(tooltipTriggerEl).addClass("tooltipified");
				return new bootstrap.Tooltip(tooltipTriggerEl);
			})
			//changeTheme();
		}
	};

	$('.card').hover(function () {
		$(this).addClass('card-zoomed');
	}, function () {
		$(this).removeClass('card-zoomed');
	});

	$('#destroy-the-page').click(function () {
		$.ajax({
			url: "/themes/custom/esahub/js/app.js",
			dataType: 'script',
			async: false
		});
	});

	//This is to force the menu close so the user can see the login pop-up nicely without having to click again. 
	$('.menu-link.cheeseburger-menu__item.account__item a[href*="user/login"]').on('click', function () {
		$("[data-cheeseburger-close]").click();
	});


})(jQuery, Drupal);