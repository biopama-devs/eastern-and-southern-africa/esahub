# ESAhub RIH theme
The Drupal 9 theme for the ESAhub RIH

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License

## Project status
Active

## References
https://www.drupal.org/node/2165673

https://www.drupal.org/docs/8/theming-drupal-8/modifying-attributes-in-a-theme-file